import { NotificationMessageDispatcherFactory } from "rokot-notification";
import { apnsInitializer, IApnsProviderOptions } from "rokot-notification/lib/apns";
import { sendgridInitializer, INodemailerSendgridOptions } from "rokot-notification/lib/nodemailer";
import { ConsoleLogger, Logger } from "rokot-log";

async function createDispatcher(logger: Logger, config: INodemailerSendgridOptions, options: IApnsProviderOptions) {
  const dispatcherFactory = new NotificationMessageDispatcherFactory(logger)
  return await dispatcherFactory.createDispatcher(sendgridInitializer(logger, config), apnsInitializer(logger, options));
}

async function runtime(sendgridOptions: INodemailerSendgridOptions, apnsOptions: IApnsProviderOptions) {
  const logger = ConsoleLogger.create("test-multiple-message-dispatcher")
  // hold dispatcher as a singleton
  const dispatcher = await createDispatcher(logger, sendgridOptions, apnsOptions)

  // dispatch a 'sample-underscore-template-message' message, targeting nodemailer (email)
  dispatcher.dispatch({ type: "sample-underscore-template-message", recipient: { nodemailer: "user@domain.com" } })

  // dispatch an 'apns' message, targeting apns
  dispatcher.dispatch({ type: "sample-apns-message", recipient: { apns: "<device token>" } })

  // dispatch a 'sample-multi-channel-message' message, targeting apns and nodemailer (email)
  dispatcher.dispatch({ type: "sample-multi-channel-message", recipient: { nodemailer: "user@domain.com", apns: "<device token>" } })

  // dispatch a 'sample-multi-channel-message' message, targeting apns
  dispatcher.dispatch({ type: "sample-multi-channel-message", recipient: { apns: "xyz" } })

  // dispatch a 'sample-multi-channel-message' message, targeting nodemailer (email is allowed as simple string values for recipient)
  dispatcher.dispatch({ type: "sample-multi-channel-message", recipient: "user@domain.com" })
}

async function test() {
  await runtime({ username: "", password: "" }, { token: { key: "", keyId: "", teamId: "" }, production: false })
}
