import { notification, INotificationMessage, reduceRecipientsEmail, reduceRecipientsToken } from "rokot-notification";
import { INodemailerMessageHandler, INodemailerNotificationTransport, createNodemailerNotification } from "rokot-notification/lib/nodemailer";
import { IApnsMessageHandler, IApnsNotificationTransport, createApnsMessage } from "rokot-notification/lib/apns";

export interface IMultipleMessage extends INotificationMessage {
  type: "sample-multi-channel-message"
  count: number;
}

export function createMessage(count: number): IMultipleMessage {
  return { type: "sample-multi-channel-message", recipient: "", count }
}

@notification.messageHandler("sample-multi-channel-message")
class MultipleHandler implements IApnsMessageHandler<IMultipleMessage>, INodemailerMessageHandler<IMultipleMessage>{

  @notification.handler()
  apns(message: IMultipleMessage, transport: IApnsNotificationTransport): Promise<any> {
    const notification = createApnsMessage(message, n => {
      n.body = `Count of ${message.count}`
      n.topic = "<app package id>"
    })
    if (notification) {
      return transport.send(notification)
    }
  }

  @notification.handler()
  nodemailer(message: IMultipleMessage, transport: INodemailerNotificationTransport) {
    const options = createNodemailerNotification(message)
    options.from = "developers@rocketmakers.com"
    options.subject = `Test Email`
    options.text = `Count of ${message.count}`
    options.html = `Test HTML email`
    return transport.send(options);
  }
}
