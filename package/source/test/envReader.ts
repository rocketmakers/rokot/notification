export type EnvKey =
  "SENDGRID" |
  "GCM" |
  "APNS" |
  "PACKAGE_ID"

export class EnvReader {
  static get(key: EnvKey): string {
    return process.env[key]
  }

  static getJson<T>(key: EnvKey): T {
    const json = EnvReader.get(key)
    if (json) {
      return JSON.parse(json) as T
    }
  }
}
