import { expect, sinon, supertest, chai } from "rokot-test";
import { NestedTemplateProcessor } from "../index";
import { createTestUnderscoreTemplateProcessorFactory, ITestTemplate } from "./templates-test";

function createTestNestedTemplateProcessor() {
  const factory = createTestUnderscoreTemplateProcessorFactory();
  return new NestedTemplateProcessor(factory);
}

describe("Nested Template Content Compiler", () => {
  it("should compile known template using known layout", () => {
    const nested = createTestNestedTemplateProcessor();
    return expect(nested.combine<ITestTemplate>("test").then(d => d({ name: "Keith" }))).to.eventually.be.fulfilled.then(html => {
      expect(html).to.eq("Head|Keith|Foot")
    });
  })

  it("should compile nested known template using known layout", () => {
    const factory = createTestUnderscoreTemplateProcessorFactory();
    const nested = new NestedTemplateProcessor(factory);
    const template = factory.create<ITestTemplate>("test");
    return expect(nested.combine(template).then(d => d({ name: "Keith" }))).to.eventually.be.fulfilled.then(html => {
      expect(html).to.eq("Head|Keith|Foot")
    });
  })

  it("should compile deeply nested known template using known layouts", () => {
    const factory = createTestUnderscoreTemplateProcessorFactory();
    const nested = new NestedTemplateProcessor(factory);
    const template = factory.create<ITestTemplate>("test");
    return expect(nested.combine(nested.combine(template, "template-layout-1")).then(d => d({ name: "Keith" }))).to.eventually.be.fulfilled.then(html => {
      expect(html).to.eq("Head|Pre|Keith|Post|Foot")
    });
  })

  it("should not compile unknown template using known layout", () => {
    const nested = createTestNestedTemplateProcessor();
    return expect(nested.combine<ITestTemplate>("x-test")).to.eventually.be.rejected;
  })

  it("should not compile known template using unknown layout", () => {
    const nested = createTestNestedTemplateProcessor();
    return expect(nested.combine<ITestTemplate>("test", "x-layout")).to.eventually.be.rejected;
  })
})
