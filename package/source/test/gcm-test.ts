import { expect, sinon, supertest, chai } from "rokot-test";
import "chai-as-promised"
import "./examples/handlers/index";
import { NotificationDispatcherFactory } from "../index";
import { gcmInitializer, IGcmProviderOptions } from "../gcm";
import { ConsoleLogger } from "rokot-log";
import { EnvReader } from "./envReader";
import { gcmToken } from "./settings";

async function createGcmDispatcher(options: IGcmProviderOptions) {
  const logger = ConsoleLogger.create("test-gcm-notification-dispatcher")
  const dispatcherFactory = new NotificationDispatcherFactory(logger)
  return await dispatcherFactory.createDispatcher(gcmInitializer(logger, options));
}

describe("GCM Notifications", () => {
  it("should send to device", async () => {
    const options = EnvReader.getJson<IGcmProviderOptions>("GCM")
    if (!options) {
      console.log("GCM not configured!!!!")
      return;
    }
    const dispather = await createGcmDispatcher(options)
    return expect(dispather.dispatch({ type: "gcm", recipient: { gcm: gcmToken } })).to.eventually.fulfilled.then(r => {
      console.log("GCM!!!! ", r)
    })
  })
})
