import * as _ from "underscore";
import { notifications, INotification } from "../../../index";
import { INodemailerNotificationHandler, INodemailerNotificationTransport } from "../../../nodemailer";

export interface IValidateFailNotification extends INotification {
}

@notifications.handler("Validate-Fail")
class ValidateFailTestEmailHandler implements INodemailerNotificationHandler<IValidateFailNotification> {
  @notifications.validator()
  validate(notification: IValidateFailNotification) {
    return Promise.reject(new Error("Not Valid"));
  }

  @notifications.transport("immediate", "never")
  async nodemailer(notification: IValidateFailNotification, transport: INodemailerNotificationTransport): Promise<any> {
    return await transport.send(notification.recipient, m => {
      m.from = transport.getFromAddress(notification)
      m.subject = `Test Email`
      m.text = `Test Text email`
      m.html = `Test HTML email`
    })
  }
}
