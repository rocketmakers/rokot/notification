import { notifications, UnderscoreFileSystemTemplateProcessorFactory, INotification } from "../../../index";
import { INodemailerNotificationHandler, INodemailerNotificationTransport } from "../../../nodemailer";
import { ISampleTemplateNotification } from "./sampleTemplateNotification";
import { emailTo } from "../../settings";
import { ConsoleLogger } from "rokot-log"
/** The example templates processor factory */
const exampleTemplatesFactory = new UnderscoreFileSystemTemplateProcessorFactory("./source/test/examples/templates", ConsoleLogger.create("Template Handler Test:"), "fragments")

export const notificationType = "sample-underscore-template-message"

/** You dont need to export the notification handler*/
@notifications.handler(notificationType)
class TemplateHandler implements INodemailerNotificationHandler<ISampleTemplateNotification>{

  /** Validate the notification before its passed to the transport method */
  @notifications.validator()
  validate(notification: ISampleTemplateNotification) {
    return Promise.resolve(notification);
  }

  /** Provides a sample notification that will be validated against any @notification.template() functions */
  @notifications.testCase()
  testMessage(): ISampleTemplateNotification {
    return { type: notificationType, recipient: { nodemailer: emailTo }, fail: true }
  }

  /** You can provide as many test cases as you like (to exercise the template generation) */
  @notifications.testCase()
  testMessage1(): ISampleTemplateNotification {
    return { type: notificationType, recipient: { nodemailer: emailTo }, fail: false }
  }

  /** You can also inspect which @notification.template() function is requesting the data */
  @notifications.testCase()
  testMessage2(template: string): ISampleTemplateNotification {
    if (template === "text") {
      // dont use this for the 'text' template
      return
    }
    return { type: notificationType, recipient: { nodemailer: emailTo } }
  }

  /**
  Using Underscore templates loaded from a root folder
  These template providing function are pre-cached at startup to reduce runtime IO
  */
  @notifications.template()
  html() {
    return exampleTemplatesFactory.create<ISampleTemplateNotification>(`${notificationType}.html`)
  }

  /** Using Underscore templates loaded from a root folder */
  @notifications.template()
  text() {
    return exampleTemplatesFactory.create<ISampleTemplateNotification>(`${notificationType}.txt`)
  }

  /** the nodemailer transport function */
  @notifications.transport()
  async nodemailer(notification: ISampleTemplateNotification, transport: INodemailerNotificationTransport) {
    return await transport.send(notification.recipient, m => {
      m.from = transport.getFromAddress(notification)
      m.subject = `Template Email`
      // using templates.transform provides access to the pre-cached @notification.template() decorated function results
      m.text = notifications.transformTemplate(notification, this.text)
      m.html = notifications.transformTemplate(notification, this.html)
    })
  }
}
