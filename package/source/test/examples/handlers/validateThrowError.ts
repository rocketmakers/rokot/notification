import * as _ from "underscore";
import { notifications, INotification } from "../../../index";
import { INodemailerNotificationHandler, INodemailerNotificationTransport } from "../../../nodemailer";
import { emailTo } from "../../settings";

export interface IValidateThrowNotification extends INotification {
  failMessage?: boolean;
}

export const notificationType = "Validate-ThrowError"

@notifications.handler(notificationType)
export class ValidateThrowErrorTestEmailHandler implements INodemailerNotificationHandler<IValidateThrowNotification> {
  static failMessage = false;

  @notifications.validator()
  validate(notification: IValidateThrowNotification) {
    // condition that will never be true!!!
    if (Date.now() < 0) {
      return Promise.resolve(notification);
    }
    throw new Error("Validate Throw");
  }

  @notifications.testCase()
  sampleMessage(): IValidateThrowNotification {
    return { type: notificationType, recipient: { nodemailer: emailTo }, failMessage: ValidateThrowErrorTestEmailHandler.failMessage }
  }

  @notifications.template()
  text() {
    return (m: IValidateThrowNotification) => _.template(m.failMessage ? 'text <%= name %>' : 'text')(m)
  }

  @notifications.transport()
  async nodemailer(notification: IValidateThrowNotification, transport: INodemailerNotificationTransport): Promise<any> {
    return await transport.send(notification.recipient, m => {
      m.from = transport.getFromAddress(notification)
      m.subject = `Test Email`
      m.text = notifications.transformTemplate(notification, this.text)
      m.html = `Test HTML email`
    })
  }
}
