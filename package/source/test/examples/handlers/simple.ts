import { notifications, INotification } from "../../../index";
import { INodemailerNotificationHandler, INodemailerNotificationTransport } from "../../../nodemailer";

/** Define the shape of the Notification */
export interface ISimpleNotification extends INotification {
  html: string;
  text: string;
}

/**
Register this handler for the notification type via @notification.handler("simple")
NOTE: You dont need to export the notification handler
*/
@notifications.handler("simple")
class SimpleHandler implements INodemailerNotificationHandler<ISimpleNotification>{

  /**
  register the nodemailer transport function via @notifications.transport()
  */
  @notifications.transport()
  async nodemailer(notification: ISimpleNotification, transport: INodemailerNotificationTransport) {
    return await transport.send(notification.recipient, m => {
      m.from = transport.getFromAddress(notification)
      m.subject = `Simple Email`
      m.text = notification.text
      m.html = notification.html
    })
  }
}
