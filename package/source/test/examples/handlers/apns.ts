import * as _ from "underscore";
import { notifications, INotification } from "../../../index";
import { IApnsNotificationHandler, IApnsNotificationTransport } from "../../../apns";

export interface ISampleApnsNotification extends INotification {
  title: string
}

@notifications.handler("sample-apns-message")
class ApnsHandler implements IApnsNotificationHandler<ISampleApnsNotification>{

  @notifications.transport()
  async apns(notification: ISampleApnsNotification, transport: IApnsNotificationTransport) {
    return await transport.send(notification.recipient, n => {
      n.alert = { body: "body", title: "title" }
      n.topic = "<app package id>"
    })
  }
}
