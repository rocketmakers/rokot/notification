import { notifications, INotification } from "../../../index";
import { INodemailerNotificationHandler, INodemailerNotificationTransport } from "../../../nodemailer";
import { IApnsNotificationHandler, IApnsNotificationTransport } from "../../../apns";

export interface IMultipleNotification extends INotification {
  type: "sample-multi-channel-message"
  count: number;
}

@notifications.handler("sample-multi-channel-message")
class MultipleHandler implements IApnsNotificationHandler<IMultipleNotification>, INodemailerNotificationHandler<IMultipleNotification>{

  /** the apns transport function */
  @notifications.transport()
  async apns(notification: IMultipleNotification, transport: IApnsNotificationTransport): Promise<any> {
    return await transport.send(notification.recipient, n => {
      n.alert = `Count of ${notification.count}`
      n.topic = "<app package id>"
    })
  }

  /** the nodemailer transport function */
  @notifications.transport()
  async nodemailer(notification: IMultipleNotification, transport: INodemailerNotificationTransport) {
    return await transport.send(notification.recipient, m => {
      m.from = transport.getFromAddress(notification)
      m.subject = `Test Email`
      m.text = `Count of ${notification.count}`
      m.html = `Test HTML email`
    })
  }
}
