import * as _ from "underscore";
import { notifications, INotification } from "../../../index";
import { INodemailerNotificationHandler, INodemailerNotificationTransport } from "../../../nodemailer";
import { emailTo } from "../../settings";

export interface ITestNotification extends INotification {
}
// const nested = new UnderscoreFileSystemNestedTemplateProcessor("./source/test/examples/templates")
// nested.transform("", "").then(r => {
//   r({})
// })

export const notificationType = "sample-test-message"
@notifications.handler(notificationType)
class TestEmailHandler implements INodemailerNotificationHandler<ITestNotification> {

  @notifications.testCase()
  testMessage(template: string): ITestNotification {
    if (template === "text") {
      return { type: notificationType, recipient: { nodemailer: emailTo } }
    }
    return { type: notificationType, recipient: { nodemailer: emailTo } }
  }

  @notifications.template()
  text() {
    const compiler = _.template('text template')
    return m => compiler(m)
  }

  @notifications.template()
  html() {
    const compiler = _.template('html template')
    return (m: ITestNotification) => compiler(m)
  }

  @notifications.transport("immediate", "weekly")
  async nodemailer(notification: ITestNotification, transport: INodemailerNotificationTransport) {
    return await transport.send(notification.recipient, m => {
      m.from = transport.getFromAddress(notification)
      m.subject = `Test Email`
      m.text = notifications.transformTemplate(notification, this.text)
      m.html = notifications.transformTemplate(notification, this.html)
    })
  }
}
