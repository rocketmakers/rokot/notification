import { INotification } from "../../../index";

export interface ISampleTemplateNotification extends INotification {
  fail?: boolean;
}
