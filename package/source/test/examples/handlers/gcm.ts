import { notifications, INotification } from "../../../index";
import { IGcmNotificationHandler, IGcmNotificationTransport } from "../../../gcm";

export interface ITestGcmNotification extends INotification {
}

@notifications.handler("gcm")
class GcmHandler implements IGcmNotificationHandler<ITestGcmNotification>{

  @notifications.transport()
  async gcm(notification: ITestGcmNotification, transport: IGcmNotificationTransport) {
    return await transport.sendMessage(notification.recipient, {
      priority: "10",
      data: {
        data: JSON.stringify({ payload: "§" }),
        title: "",
        message: "",
        smallIcon: "",
        id: "",
        sound: "",
        number: "1",
      }
    })
  }
}
