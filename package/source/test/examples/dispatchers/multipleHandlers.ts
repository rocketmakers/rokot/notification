import { NotificationDispatcherFactory } from "../../../index";
import { apnsInitializer, ProviderOptions } from "../../../apns";
import { sendgridInitializer, INodemailerSendgridOptions } from "../../../nodemailer";
import { gcmInitializer, IGcmProviderOptions } from "../../../gcm";
import { ConsoleLogger } from "rokot-log";

async function createDispatcher(nodemailerOptions: INodemailerSendgridOptions, apnsOptions: ProviderOptions, gcmOptions: IGcmProviderOptions) {
  const logger = ConsoleLogger.create("test-multiple-message-dispatcher")
  const dispatcherFactory = new NotificationDispatcherFactory(logger)
  return await dispatcherFactory.createDispatcher(
    sendgridInitializer(logger, nodemailerOptions),
    gcmInitializer(logger, gcmOptions),
    apnsInitializer(logger, apnsOptions));
}

// createDispatcher({username:"sendgrid username", password:"send grid password"}, {token:{key:"/path/to.p8",keyId:"KEY_ID",teamId:"TEAM_ID"},production:false}, {apiKey:"<gcm api key>"})
