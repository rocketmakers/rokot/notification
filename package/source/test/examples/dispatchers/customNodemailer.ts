import { NotificationDispatcherFactory } from "../../../index";
import { NodemailerTransporterBase, nodemailerInitializer } from "../../../nodemailer";
import { ConsoleLogger, Logger } from "rokot-log";
import { emailTo } from "../../settings";

async function createDispatcher(logger: Logger, transporterProvider: NodemailerTransporterBase) {
  const dispatcherFactory = new NotificationDispatcherFactory(logger, m => new m.HandlerClass())
  return await dispatcherFactory.createDispatcher(nodemailerInitializer(logger, transporterProvider, "developers@rocketmakers.com"))
}

interface ICustomNodemailerConfig {
  username: string;
  password: string;
}

/**
Load your custom transport
e.g. const customTransport = require('nodemailer-custom-transport');
*/
const customTransport: any = () => { }

/** Nodemailer specific: Implementation of the nodemailer Sendgrid Transporter plugin */
class NodemailerCustomTransporter extends NodemailerTransporterBase {
  constructor(private logger: Logger, private customConfig: ICustomNodemailerConfig) {
    super()
  }

  protected validate() {
    this.logger.trace(`Validating Nodemailer Custom Transporter Config`)
    if (!this.customConfig || !this.customConfig.username || !this.customConfig.password) {
      const msg = "credential cannot be null/undefined or contain empty username and/or password";
      this.logger.error("-- " + msg)
      throw new Error(msg)
    }
  }

  protected createNodemailerTransport() {
    return customTransport({
      auth: {
        username: this.customConfig.username,
        password: this.customConfig.password
      }
    })
  }
}

async function test() {
  const logger = ConsoleLogger.create("test-custom-nodemailer-message-dispatcher")
  const transporter = new NodemailerCustomTransporter(logger, { username: "", password: "" })
  const dispatcher = await createDispatcher(logger, transporter)

  dispatcher.dispatch({ type: "template", recipient: { nodemailer: emailTo } })
}
