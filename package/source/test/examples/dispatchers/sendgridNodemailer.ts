import { NotificationDispatcherFactory } from "../../../index";
import { sendgridInitializer, INodemailerSendgridOptions } from "../../../nodemailer";
import { ConsoleLogger, Logger } from "rokot-log";
import { emailTo } from "../../settings";

async function createDispatcher(logger: Logger, config: INodemailerSendgridOptions) {
  // hold dispatcher as a singleton
  const dispatcherFactory = new NotificationDispatcherFactory(logger)
  return await dispatcherFactory.createDispatcher(sendgridInitializer(logger, config));
}

async function test() {
  const logger = ConsoleLogger.create("test-sendgrid-nodemailer-message-dispatcher")
  const dispatcher = await createDispatcher(logger, { username: "", password: "", defaultFromAddress: "developers@rocketmakers.com" })

  dispatcher.dispatch({ type: "my", recipient: { nodemailer: emailTo } })
}
