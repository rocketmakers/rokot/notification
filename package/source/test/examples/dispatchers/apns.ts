import { NotificationDispatcherFactory } from "../../../index";
import { apnsInitializer, ProviderOptions } from "../../../apns";
import { ConsoleLogger, Logger } from "rokot-log";
import { apnsToken } from "../../settings";

async function createDispatcher(logger: Logger, options: ProviderOptions) {
  const dispatcherFactory = new NotificationDispatcherFactory(logger)
  return await dispatcherFactory.createDispatcher(apnsInitializer(logger, options));
}

async function test() {
  const logger = ConsoleLogger.create("test-apns-message-dispatcher")
  const dispatcher = await createDispatcher(logger, { token: { key: "", keyId: "", teamId: "" }, production: false })

  dispatcher.dispatch({ type: "sample-apns-message", recipient: { apns: apnsToken } })
}
