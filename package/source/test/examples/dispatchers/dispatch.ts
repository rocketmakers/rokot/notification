import { INotificationDispatcher } from "../../../index";
import { emailTo, apnsToken } from "../../settings";


async function test(dispatcher: INotificationDispatcher) {
  // dispatch a 'sample-underscore-template-message' message, targeting nodemailer (email)
  dispatcher.dispatch({ type: "sample-underscore-template-message", recipient: { nodemailer: emailTo } })

  // dispatch an 'apns' message, targeting apns
  dispatcher.dispatch({ type: "sample-apns-message", recipient: { apns: apnsToken } })

  // dispatch a 'sample-multi-channel-message' message, targeting apns and nodemailer (email)
  dispatcher.dispatch({ type: "sample-multi-channel-message", recipient: { nodemailer: emailTo, apns: apnsToken } })

  // dispatch a 'sample-multi-channel-message' message, targeting apns
  dispatcher.dispatch({ type: "sample-multi-channel-message", recipient: [{ apns: apnsToken }] })

  // dispatch a 'sample-multi-channel-message' message, targeting nodemailer (email is allowed as simple string values for recipient)
  dispatcher.dispatch({ type: "sample-multi-channel-message", recipient: emailTo })
}
