import { ISampleApnsNotification } from "./handlers/apns"
import { ITestGcmNotification } from "./handlers/gcm"
import { IMultipleNotification } from "./handlers/multiple"
import { ISimpleNotification } from "./handlers/simple"
import { ISampleTemplateNotification } from "./handlers/sampleTemplateNotification"
import { ITestNotification } from "./handlers/test"
import { IValidateFailNotification } from "./handlers/validateFail"
import { IValidateThrowNotification } from "./handlers/validateThrowError"
import { NotificationRecipients, INotificationDispatcher, INotificationDispatchResult } from "../../index"

export class Notifications {
  constructor(private dispatcher: INotificationDispatcher) { }
  sampleApnsNotification(recipient: NotificationRecipients, payload: { title: string }): Promise<INotificationDispatchResult[]> {
    return this.dispatcher.dispatch({ type: "sample-apns-message", recipient, title: payload.title } as ISampleApnsNotification)
  }
  testGcmNotification(recipient: NotificationRecipients): Promise<INotificationDispatchResult[]> {
    return this.dispatcher.dispatch({ type: "gcm", recipient } as ITestGcmNotification)
  }
  multipleNotification(recipient: NotificationRecipients, payload: { count: number }): Promise<INotificationDispatchResult[]> {
    return this.dispatcher.dispatch({ type: "sample-multi-channel-message", recipient, count: payload.count } as IMultipleNotification)
  }
  simpleNotification(recipient: NotificationRecipients, payload: { html: string, text: string }): Promise<INotificationDispatchResult[]> {
    return this.dispatcher.dispatch({ type: "simple", recipient, html: payload.html, text: payload.text } as ISimpleNotification)
  }
  sampleTemplateNotification(recipient: NotificationRecipients, payload: { fail?: boolean }): Promise<INotificationDispatchResult[]> {
    return this.dispatcher.dispatch({ type: "sample-underscore-template-message", recipient, fail: payload.fail } as ISampleTemplateNotification)
  }
  testNotification(recipient: NotificationRecipients): Promise<INotificationDispatchResult[]> {
    return this.dispatcher.dispatch({ type: "sample-test-message", recipient } as ITestNotification)
  }
  validateFailNotification(recipient: NotificationRecipients): Promise<INotificationDispatchResult[]> {
    return this.dispatcher.dispatch({ type: "Validate-Fail", recipient } as IValidateFailNotification)
  }
  validateThrowNotification(recipient: NotificationRecipients, payload: { failMessage?: boolean }): Promise<INotificationDispatchResult[]> {
    return this.dispatcher.dispatch({ type: "Validate-ThrowError", recipient, failMessage: payload.failMessage } as IValidateThrowNotification)
  }
}
