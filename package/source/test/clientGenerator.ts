import { ConsoleLogger } from "rokot-log";
import { generateClient, getInterfaceFunctionName, getFolderFiles } from "../client/generator";
const logger = ConsoleLogger.create("create notification client", { level: "info" })

/** Get all notification handler source files (all in a single folder) */
const files = getFolderFiles("./source/test/examples/handlers");

/** Generate a notification client */
generateClient(logger, files, "./source/test/examples/notifications.ts", {
  /** override the method naming function */
  functionName: getInterfaceFunctionName
})
