import { expect, sinon, supertest, chai } from "rokot-test";
import { ConsoleLogger } from "rokot-log";
import { TemplatePreProcessor, messageHandlerDecoration } from "../index";
import { ValidateThrowErrorTestEmailHandler } from "./examples/handlers/validateThrowError";
import "chai-as-promised"
import "./examples/handlers/index";

const logger = ConsoleLogger.create("template-pre-processor", { level: "trace" });

describe("Template Pre Processor", () => {
  it("should collect and validate templates", () => {
    const vv = new TemplatePreProcessor(logger)
    ValidateThrowErrorTestEmailHandler.failMessage = false;
    return expect(vv.preProcess(messageHandlerDecoration.getAll())).to.eventually.be.fulfilled.then(t => {
      console.log("COMPILED TEMPLATES:", t);
      return t;
    });
  })

  it("should collect and fail to validate templates", () => {
    const vv = new TemplatePreProcessor(logger)
    ValidateThrowErrorTestEmailHandler.failMessage = true;
    return expect(vv.preProcess(messageHandlerDecoration.getAll())).to.eventually.be.rejected.then(err => {
      ValidateThrowErrorTestEmailHandler.failMessage = false;
      console.log("COMPILED TEMPLATES ERROR:", err);
    });
  })
})
