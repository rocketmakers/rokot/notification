import { expect, sinon, supertest, chai } from "rokot-test";
import * as _ from "underscore";
import "chai-as-promised"
import "./examples/handlers/index";
import { NotificationDispatcherFactory } from "../index";
import { apnsInitializer, ProviderOptions } from "../apns";
import { ConsoleLogger } from "rokot-log";
import { EnvReader } from "./envReader";
import { apnsToken } from "./settings";

async function createApnsDispatcher(options: ProviderOptions) {
  const logger = ConsoleLogger.create("test-apns-message-dispatcher")
  const dispatcherFactory = new NotificationDispatcherFactory(logger)
  return await dispatcherFactory.createDispatcher(apnsInitializer(logger, options));
}

describe("APNS Notifications", () => {
  it("should send to device", async () => {
    const apns = EnvReader.getJson<ProviderOptions>("APNS")
    if (!apns) {
      console.log("APNS not configured!!!!")
      return;
    }
    const dispatcher = await createApnsDispatcher(apns)
    return expect(dispatcher.dispatch({ type: "sample-apns-message", recipient: { apns: apnsToken } })).to.eventually.fulfilled.then(r => {
      console.log("APNS!!!!", r)
    })
  })
})
