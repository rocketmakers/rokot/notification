import { expect, sinon, supertest, chai } from "rokot-test";
import * as Sinon from "sinon";
import { ConsoleLogger } from "rokot-log";
import { messageHandlerDecoration, INotification, NotificationRecipients, INotificationDispatchResult } from "../index";
import { INodemailerNotification, INodemailerNotificationTransport, NodemailerNotificationDispatchHandler } from "../nodemailer";
import "chai-as-promised"
import * as nodemailer from "nodemailer";
import { NotificationDispatcherFactory } from "../index";
const logger = ConsoleLogger.create("nodemailer", { level: "trace" });
import "./examples/handlers/index";
import { emailTo } from "./settings";

class TestTransport implements INodemailerNotificationTransport {
  emailStub: Sinon.SinonStub;
  constructor(promise = Promise.resolve({})) {
    this.emailStub = sinon.stub().returns(promise);
  }
  native: any = {}

  getFromAddress(notification: INodemailerNotification): string {
    return notification.fromAddress || "info@rocketmakers.com"
  }

  async resolveTokens(recipients: NotificationRecipients): Promise<string[]> {
    return []
  }

  async send(recipients: NotificationRecipients, builder?: (message: nodemailer.SendMailOptions) => void): Promise<INotificationDispatchResult> {
    return this.emailStub({})
  }

  shutdown() { }
}

function createDispatcher(transport: INodemailerNotificationTransport = new TestTransport()) {
  const dispatcherFactory = new NotificationDispatcherFactory(logger, c => new c.HandlerClass())
  return dispatcherFactory.createDispatcher(new NodemailerNotificationDispatchHandler(logger, transport))
}

function expectTestOk(msg: INotification, stubExpect: (stub: Sinon.SinonStub) => void) {
  const tt = new TestTransport();
  return expect(createDispatcher(tt).then(proc => proc.dispatch(msg))).to.eventually.be.fulfilled.then(() => {
    stubExpect(tt.emailStub)
  })
}

function expectTestSendFail(msg: INotification, stubExpect: (stub: Sinon.SinonStub) => void) {
  const tt = new TestTransport(Promise.reject("Force send fail"));
  return expect(createDispatcher(tt)
    .then(proc => proc.dispatch(msg))).to.eventually.be.rejected.then(() => {
      stubExpect(tt.emailStub)
    })
}

function expectProcessFail(msg: INotification, expectError: string) {
  const tt = new TestTransport();
  return expect(createDispatcher(tt).then(proc => proc.dispatch(msg))).to.eventually.be.rejected.then(e => {
    expect(e.message.indexOf(expectError)).to.gt(-1)
  })
}

describe("Notification Message Metadata Collector", () => {
  it("should collect metadata", () => {
    const all = messageHandlerDecoration.getAll()
    console.log("COLLECTION", JSON.stringify(all, null, 2))
  })
})

function createMessage(type: string) {
  return { type, recipient: { nodemailer: emailTo } }
}

describe("nodemailer", () => {
  it("should pass with valid email and type", () => {
    const msg = createMessage("sample-test-message");
    return expectTestOk(msg, s => {
      expect(s.calledOnce).to.be.true;
      expect(s.firstCall.args.length).to.eq(1)
      //expect(s.firstCall.args[0]).to.eq(msg)
    })
  })
  it("should fail with valid email and type when send fails", () => {
    return expectTestSendFail(createMessage("sample-test-message"), s => {
      expect(s.calledOnce).to.be.true;
    })
  })

  it("should fail to find unregistered notification handler", () => {
    return expectProcessFail(createMessage("test1"), `type: test1`)
  })

  it("should fail when validate rejected", () => {
    return expectProcessFail(createMessage("Validate-Fail"), "Not Valid")
  })

  it("should fail when validate throws an exception", () => {
    return expectProcessFail(createMessage("Validate-ThrowError"), "Validate Throw")
  })
})
