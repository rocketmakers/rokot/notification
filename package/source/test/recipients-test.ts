import { expect, sinon, supertest, chai } from "rokot-test";
import { recipientTokenResolver, NotificationRecipients } from "../index";

function testEmail(recipients: NotificationRecipients, expected: string[], tokenNames: string | string[] = "email") {
  const emailsPromise = recipientTokenResolver.find(recipients, tokenNames, true)
  expect(emailsPromise).to.fulfilled.then(emails => {
    expect(emails.length).to.eq(expected.length)
    for (let i = 0; i < emails.length; i++) {
      expect(emails[i]).to.eq(expected[i])
    }
  })
}

function testToken(recipients: NotificationRecipients, type: string, expected: string[]) {
  const tokensPromise = recipientTokenResolver.find(recipients, type)
  expect(tokensPromise).to.fulfilled.then(tokens => {
    expect(tokens.length).to.eq(expected.length)
    for (let i = 0; i < tokens.length; i++) {
      expect(tokens[i]).to.eq(expected[i])
    }
  })
}

describe("Reducing Recipients", () => {
  it("should extract email for INotificationRecipient", () => {
    const emailAddress = "email address";
    testEmail({ email: emailAddress }, [emailAddress])
  })

  it("should extract email as fallback for INotificationRecipient", () => {
    const emailAddress = "email address";
    testEmail({ email: emailAddress }, [emailAddress], ["nodemailer", "email"])
  })

  it("should extract nodemailer as primary for INotificationRecipient", () => {
    const emailAddress = "email address";
    testEmail({ nodemailer: emailAddress, email: "never used" }, [emailAddress], ["nodemailer", "email"])
  })

  it("should extract email for INotificationRecipient array", () => {
    const emailAddress = "email address";
    testEmail([{ email: emailAddress }], [emailAddress])
  })

  it("should extract email for string", () => {
    const emailAddress = "email address";
    testEmail(emailAddress, [emailAddress])
  })

  it("should extract email for string array", () => {
    const emailAddress = "email address";
    testEmail([emailAddress], [emailAddress])
  })

  it("should extract 'apns' token for INotificationRecipient with token array", () => {
    const emailAddress = "email address";
    const token = "Apple Token";
    const token1 = "Apple Token1";
    testToken({ email: emailAddress, apns: [token, token1] }, "apns", [token, token1])
  })

  it("should extract 'apns' token for INotificationRecipient array with tokens", () => {
    const emailAddress = "email address";
    const token = "Apple Token";
    testToken([{ email: emailAddress, apns: token }], "apns", [token])
  })

  it("should not extract 'missing-type' token for INotificationRecipient with missing type", () => {
    const emailAddress = "email address";
    const token = "Apple Token";
    testToken({ email: emailAddress, apns: token }, "missing-type", [])
  })

  it("should not extract 'missing-type' token for INotificationRecipient array with missing types", () => {
    const emailAddress = "email address";
    const token = "Apple Token";
    testToken([{ email: emailAddress, apns: token }], "missing-type", [])
  })

  it("should not extract 'apns' token for NotificationRecipients as string", () => {
    const emailAddress = "email address";
    testToken(emailAddress, "apns", [])
  })

  it("should not extract 'apns' token for NotificationRecipients as string array", () => {
    const emailAddress = "email address";
    testToken([emailAddress], "apns", [])
  })
})
