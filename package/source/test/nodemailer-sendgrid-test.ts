import { expect, sinon, supertest, chai } from "rokot-test";
import { ConsoleLogger } from "rokot-log";
import { INodemailerSendgridOptions } from "../nodemailer";
import "chai-as-promised"
import { NotificationDispatcherFactory } from "../index";
import "./examples/handlers/index";
import { EnvReader } from "./envReader";
import { sendgridInitializer } from "../nodeMailer/sendgrid/initializer";

const logger = ConsoleLogger.create("nodemailer-sendgrid", { level: "trace" });

function getCredentials(): INodemailerSendgridOptions {
  return EnvReader.getJson<INodemailerSendgridOptions>("SENDGRID")
}

function expectInitFail(config: INodemailerSendgridOptions, expectError: string) {
  const dispatcherFactory = new NotificationDispatcherFactory(logger)
  const dispatcherPromise = dispatcherFactory.createDispatcher(sendgridInitializer(logger, config));
  return expect(dispatcherPromise).to.eventually.be.rejected.then(e => {
    expect(e.message.indexOf(expectError)).to.gt(-1)
  })
}

describe("nodemailer sendgrid", () => {
  // it("should fail init with missing credentials", () => {
  //   return expectInitFail(undefined, "contain empty username")
  // })

  it("should fail init with 'empty' password credential", () => {
    return expectInitFail({ username: "a", password: "", defaultFromAddress: "developers@rocketmakers.com" }, "contain empty username")
  })
})
