import { expect, sinon, supertest, chai } from "rokot-test";
import { UnderscoreTemplateProcessorFactory, ITemplateContentProvider } from "../index";

export interface ITestTemplate {
  name: string;
}

function getTestTemplate(type: string) {
  switch (type) {
    case "template-layout":
      return `Head|<%= model.body %>|Foot`
    case "template-layout-1":
      return `Pre|<%= model.body %>|Post`
    case "test":
      return `<%= model.name %>`
    case "test1":
      return `|<%= model.name %>|`
    case "test-bad":
      return `|<%= unknownUnderscoreProperty %>|`
  }
}

class TestTemplateContentProvider implements ITemplateContentProvider {
  get(type: string): Promise<string> {
    const template = getTestTemplate(type)
    if (template) {
      return Promise.resolve(template)
    }

    return Promise.reject(new Error(`Unable to find template for type '${type}'`))
  }
}
export function createTestUnderscoreTemplateProcessorFactory() {
  return new UnderscoreTemplateProcessorFactory(new TestTemplateContentProvider());
}

describe("Underscore Compiled Template Provider", () => {
  it("should compile known template", () => {
    const factory = createTestUnderscoreTemplateProcessorFactory();
    const p = expect(factory.create<ITestTemplate>("test1")).to.eventually.be.fulfilled;
    return expect(p.then(d => d({ name: "Keith" }))).to.eventually.be.fulfilled.then(html => {
      expect(html).to.eq("|Keith|")
    });
  })

  it("should compile known template, but fail to render due to unknown property in template", () => {
    const factory = createTestUnderscoreTemplateProcessorFactory();
    const p = expect(factory.create<ITestTemplate>("test-bad")).to.eventually.be.fulfilled;
    return expect(p.then(m => m({ name: "Keith" }))).to.eventually.be.rejected.then(e => {
      expect(e.message.indexOf("unknownUnderscoreProperty")).to.gt(-1)
    });
  })

  it("should not compile unknown template", () => {
    const factory = createTestUnderscoreTemplateProcessorFactory();
    return expect(factory.create<ITestTemplate>("x-test")).to.eventually.be.rejected;
  })
})
