export { sendgridInitializer, ISendgridOptions, SendgridTransporter, SendgridNotificationTransport, SendgridNotificationDispatchHandler, ISendgridNotificationTransport, ISendgridNotificationHandler, ISendgridNotification } from "./sendgrid/plugin";
export { MailData } from "@sendgrid/helpers/classes/mail";
export { EmailData } from "@sendgrid/helpers/classes/email-address";

