import { Message, Sender, IMessageOptions } from "node-gcm";
import * as Logger from "bunyan";
import { IRecipientResolver, recipientTokenResolver } from "../core/recipients";
import { INotification, NotificationRecipients, INotificationDispatchResult, INotificationHandler, INotificationTransport } from "../core/core";
import { NotificationDispatchHandler } from "../core/dispatchHandler";

declare module "../core/core" {
  interface INotificationRecipient {
    gcm?: string | string[]
  }
}

/** Implement this interface onto notification handlers that use the gcm transport  */
export interface IGcmNotificationHandler<T extends INotification> extends INotificationHandler<T> {
  gcm(notification: T, transport: IGcmNotificationTransport): Promise<any>
}

export interface IGcmNotificationTransport extends INotificationTransport<Sender> {
  /** If the recipients have valid tokens, creates an gcm message and sends over the gcm transport, otherwise undefined  */
  sendMessage(recipients: NotificationRecipients, options: IMessageOptions): Promise<INotificationDispatchResult>
}

export interface IGcmProviderOptions {
  apiKey: string
}

export async function gcmInitializer(logger: Logger, options: IGcmProviderOptions, recipientResolver: IRecipientResolver = recipientTokenResolver) {
  const transport = await GcmNotificationTransport.create(logger, options, recipientResolver);
  return new GcmNotificationDispatchHandler(logger, transport)
}


export class GcmNotificationDispatchHandler extends NotificationDispatchHandler<IGcmNotificationTransport> {
  constructor(logger: Logger, transport: IGcmNotificationTransport) {
    super("gcm", logger, transport)
  }
}

/** The Transport passed to notification handlers */
export class GcmNotificationTransport implements IGcmNotificationTransport {

  static async create(logger: Logger, options: IGcmProviderOptions, recipientResolver: IRecipientResolver): Promise<IGcmNotificationTransport> {
    const provider = new Sender(options.apiKey);
    return new GcmNotificationTransport(logger, provider, recipientResolver);
  }

  constructor(private logger: Logger, public native: Sender, public recipientResolver: IRecipientResolver) {
    logger.trace("Created Gcm Notification Transport")
  }

  resolveTokens(recipients: NotificationRecipients): Promise<string[]> {
    return this.recipientResolver.find(recipients, "gcm")
  }

  async sendMessage(recipients: NotificationRecipients, options: IMessageOptions) {
    const tokens = await this.resolveTokens(recipients)
    if (!tokens || !tokens.length) {
      return
    }

    return this.send(tokens, new Message(options))
  }

  private async send(tokens: string[], message: Message): Promise<INotificationDispatchResult> {
    return new Promise<INotificationDispatchResult>((res, rej) => {
      this.native.send(message, tokens, (err, resp) => {
        //resp.results[0].
        if (err) {
          this.logger.error(err, "GCM was unable to send message", message);
          return res({ transport: "gcm", error: err });
        }
        this.logger.trace("GCM Send response: ", JSON.stringify(resp, null, 2))
        const sent = resp.results ? resp.results.filter(r => !r.error).map(r => r.registration_id) : []
        const failed = resp.results ? resp.results.filter(r => r.error).map(r => ({ token: r.registration_id, error: r.error })) : []
        res({ transport: "gcm", sent, failed })
      })
    })
  }

  shutdown() {
    //this.native.shutdown()
  }
}
