import { INotification, INotificationHandler, INotificationDispatchHandler, INotificationTransport, INotificationDispatchResult } from "./core";
import * as Logger from "bunyan";
import * as _ from "underscore";

/**
The Default NotificationDispatchHandler behavior
where the NotificationHandler is invoked with the notification and the transport
*/
export class NotificationDispatchHandler<TTransport extends INotificationTransport<any>> implements INotificationDispatchHandler {
  constructor(public transportName: string, private logger: Logger, private transport: TTransport) {
  }

  onDispatch(notification: INotification, messageHandler: INotificationHandler<any>): Promise<INotificationDispatchResult> {
    if (this.canHandleMessage(messageHandler)) {
      return this.invokeMessage(notification, messageHandler)
    }
  }

  /** checks the messageHandler implements the correct method name */
  protected canHandleMessage(messageHandler: INotificationHandler<any>): boolean {
    return _.isFunction(messageHandler[this.transportName]);
  }

  /**
  invokes the NotificationHandler method, passing the notification and the transport
  */
  protected invokeMessage(notification: INotification, messageHandler: INotificationHandler<any>) {
    return messageHandler[this.transportName](notification, this.transport)
  }

  /** Shutdown the transport */
  shutdown() {
    this.transport.shutdown()
  }
}
