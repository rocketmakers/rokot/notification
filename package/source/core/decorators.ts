import 'reflect-metadata';
import { Frequency, INotificationHandler, INewable, INotificationHandlerDecoration, INotificationDispatchHandlerTransport } from "./core";
import { MetadataKeys } from "./metadataKeys";

export interface IMessageHandlerRegistry {
  get(type: string): INotificationHandlerDecoration;
  getAll(): INotificationHandlerDecoration[];
}

class MessageHandlerRegistry implements IMessageHandlerRegistry {
  private items: INotificationHandlerDecoration[] = []
  get(type: string) {
    const item = this.items.filter(i => i.notificationType === type);
    if (item.length) {
      return item[0]
    }
  }
  getAll() {
    return this.items;
  }

  register(notificationType: string, name: string, group: string, HandlerClass: INewable<INotificationHandler<any>>, transports: INotificationDispatchHandlerTransport[], validators: string[], testCases: string[], templates: string[]) {
    this.items.push({ notificationType, name, group, HandlerClass, transports, validators, testCases, templates });
  }
}

const registry = new MessageHandlerRegistry();
export const messageHandlerDecoration: IMessageHandlerRegistry = registry;

function getMessageValidators(target: any): string[] {
  return Reflect.getMetadata(MetadataKeys.templateMessageValidators, target) || [];
}

function getNotificationTestCases(target: any): string[] {
  return Reflect.getMetadata(MetadataKeys.notificationTestCase, target) || [];
}

function getCompilers(target: any): string[] {
  return Reflect.getMetadata(MetadataKeys.templateCompiler, target) || [];
}

export class Notification {
  /** Place onto a class to mark it as being a notification handler */
  handler(type: string, name: string = `MessageHandler-${type}`, group: string = ""): ClassDecorator {
    return function (target: Function) {
      const frequencies = Reflect.getMetadata(MetadataKeys.frequency, target.prototype);
      const validators = getMessageValidators(target.prototype);
      const testCases = getNotificationTestCases(target.prototype);
      const templates = getCompilers(target.prototype);

      registry.register(type, name, group, target as INewable<INotificationHandler<any>>, frequencies, validators, testCases, templates);
    }
  }

  /**
  Defines a function that provides template content
  The decorated function should return ITemplateProcessor<T> | Promise<ITemplateProcessor<T>>
  */
  template() {
    return (target: any, methodName: string, descriptor?: PropertyDescriptor) => {
      const compilers = getCompilers(target);
      compilers.push(methodName);
      Reflect.defineMetadata(MetadataKeys.templateCompiler, compilers, target);
    }
  }

  transformTemplate<T>(notification: T, func: Function): string {
    const tp = Reflect.getMetadata(MetadataKeys.templateProcessor, func)
    if (!tp) {
      throw new Error("Is the function being passed in decorated with @notification.template()")
    }
    return tp(notification);
  }

  /**
  Defines a function that provides an example notification for template content
  The decorated function will be
    passed the name of the function attributed with @notification.template()
    should return the INotification required
  */
  testCase() {
    return (target: any, methodName: string, descriptor?: PropertyDescriptor) => {
      const testCases = getNotificationTestCases(target);
      testCases.push(methodName);
      Reflect.defineMetadata(MetadataKeys.notificationTestCase, testCases, target);
    }
  }

  /**
  Defines a function that validates messages
  The decorated function
    will be passed the notification and
    should return the validated INotification within a Promise (message: TNotification) => Promise<TNotification>
    should reject the promise on failure
  */
  validator() {
    return (target: any, methodName: string, descriptor?: PropertyDescriptor) => {
      const templateMessageValidators = getMessageValidators(target)
      templateMessageValidators.push(methodName);
      Reflect.defineMetadata(MetadataKeys.templateMessageValidators, templateMessageValidators, target);
    }
  }

  /**
  Denotes a transport function
  Set the Frequency intent of the notification
  The first option supplied is the default, the following options are valid alternatives
  Defaults to "immediate"
  */
  transport(...availableFrequencies: Frequency[]) {
    if (!availableFrequencies.length) {
      availableFrequencies.push("immediate");
    }
    return function (target: any, methodName: string, descriptor?: PropertyDescriptor) {
      const frequencies: INotificationDispatchHandlerTransport[] = Reflect.getMetadata(MetadataKeys.frequency, target) || [];
      frequencies.push({ methodName, defaultFrequency: availableFrequencies[0], availableFrequencies })
      Reflect.defineMetadata(MetadataKeys.frequency, frequencies, target);
    }
  }
}

export const notifications = new Notification()
