import 'reflect-metadata';
import { MetadataKeys } from "./metadataKeys";
import { ITemplateProcessor, ITemplateProcessorProviderFunction } from "../templates/core";


export class Templates {
  /** stores a cached version of the template function output in reflect metadata */
  set<T>(proc: ITemplateProcessor<T>, func: ITemplateProcessorProviderFunction<T>) {
    Reflect.defineMetadata(MetadataKeys.templateProcessor, proc, func)
  }
}

export const templates = new Templates()
