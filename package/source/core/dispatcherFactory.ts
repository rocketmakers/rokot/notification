import * as Logger from "bunyan";
import { PromiseOrResult, INotificationDispatchHandler, INotificationHandlerConstructor } from "./core";
import { resolvePromiseOrResult } from "./common";
import { NotificationDispatcher } from "./dispatcher";
import { TemplatePreProcessor } from "../templates/templatePreProcessor";
import { messageHandlerDecoration } from "./decorators";
/** The Entry Point to creating a Notification Message Processor */
export class NotificationDispatcherFactory {
  constructor(protected logger: Logger, private handlerClassConstructor?: INotificationHandlerConstructor) { }

  /** Create the Notification Message Processor using the supplied initializers */
  async createDispatcher(...initializer: PromiseOrResult<INotificationDispatchHandler>[]): Promise<NotificationDispatcher> {
    const preProc = new TemplatePreProcessor(this.logger, this.handlerClassConstructor)
    await preProc.preProcess(messageHandlerDecoration.getAll())

    const dispatchHandlers = await this.resolveNotificationDispatchHandlers(initializer)

    this.logger.trace(`-- Creating Notification Message Processor`)
    return new NotificationDispatcher(this.logger, dispatchHandlers, this.handlerClassConstructor);
  }

  private async resolveNotificationDispatchHandlers(initializer: PromiseOrResult<INotificationDispatchHandler>[]) {
    this.logger.trace(`Resolving Notification Message Dispatch Handlers`)
    const dispatchHandlers: INotificationDispatchHandler[] = []
    for (const init of initializer) {
      const dd = await resolvePromiseOrResult(init)
      this.logger.trace(`-- Loaded ${dd.transportName}`)
      dispatchHandlers.push(dd)
    }
    return dispatchHandlers
  }
}
