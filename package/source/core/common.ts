import { INotification, NotificationRecipients, PromiseOrResult, INotificationHandlerDecoration, INotificationHandlerConstructor } from "./core";
import * as _ from "underscore";

export function createNotificationMessage<T extends { type: string }>(recipient: NotificationRecipients, payload: T): T & INotification {
  return _.extend({ recipient }, payload) as T & INotification
}

export async function resolvePromiseOrResult<T>(promise: PromiseOrResult<T>) {
  if (isPromise(promise)) {
    return await promise
  }
  return promise;
}

export function isPromise<T>(promiseOrType: PromiseOrResult<T>): promiseOrType is Promise<T> {
  return promiseOrType && _.isFunction((promiseOrType as Promise<T>).then);
}

export function constructNotificationMessageHandler(decoration: INotificationHandlerDecoration, handlerClassConstructor?: INotificationHandlerConstructor) {
  if (handlerClassConstructor) {
    return handlerClassConstructor(decoration);
  }

  return new decoration.HandlerClass();
}
