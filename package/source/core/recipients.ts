import { NotificationRecipients, INotificationRecipient, ItemOrArray } from "./core";
import * as _ from "underscore";

export interface IRecipientResolver {
  find(recipients: NotificationRecipients, tokenName: ItemOrArray<string>, acceptString?: boolean): Promise<string[]>
}

function resolveRecipients(acceptString: boolean, recipients: NotificationRecipients, mapper: (t: INotificationRecipient) => ItemOrArray<string>): string[] {
  if (!recipients) {
    return []
  }
  const recipientsArray = _.isArray(recipients) ? recipients : [recipients]
  const tokens: string[] = []
  recipientsArray.forEach(r => {
    if (_.isString(r)) {
      if (acceptString) {
        tokens.push(r)
      }
      return
    }

    const mapped = mapper(r)
    if (!mapped) {
      return;
    }
    if (_.isArray(mapped)) {
      tokens.push(...mapped.filter(m => !!m))
    } else if (mapped) {
      tokens.push(mapped)
    }
  })
  return tokens
}

function findRecipientToken(recipient: INotificationRecipient, tokenName: ItemOrArray<string>) {
  if (_.isArray(tokenName)) {
    for (let name of tokenName) {
      let token = recipient[name]
      if (token) {
        return token
      }
    }
    return
  }

  return recipient[tokenName]
}

class RecipientTokenResolver implements IRecipientResolver {
  async find(recipients: NotificationRecipients, tokenName: ItemOrArray<string>, acceptString = false) {
    return resolveRecipients(acceptString, recipients, r => findRecipientToken(r, tokenName))
  }
}

/** Resolves string values as an acceptable option (and ignores the passed tokenName) */
export const recipientTokenResolver: IRecipientResolver = new RecipientTokenResolver()
