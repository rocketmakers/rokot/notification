export class MetadataKeys {
  static notificationTestCase = "notificationTestCase"
  static templateMessageValidators = "templateMessageValidators"
  static templateCompiler = "templateCompiler"
  static templateProcessor = "templateProcessor"
  static frequency = "frequency"
}
