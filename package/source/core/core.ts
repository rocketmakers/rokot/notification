export type Frequency = "immediate" | "daily" | "weekly" | "never";

export const AllFrequency: Frequency[] = ["immediate", "daily", "weekly", "never"];

export type PromiseOrResult<T> = Promise<T> | T
export type ItemOrArray<T> = T | T[]

export interface INewable<T> {
  new(...args: any[]): T
}

export interface INotificationDispatchHandlerTransport {
  methodName: string;
  defaultFrequency: string;
  availableFrequencies: string[];
}

export interface INotificationHandlerDecoration {
  /** The actual notification handler constructor (not serializable)*/
  HandlerClass?: INewable<INotificationHandler<any>>

  /** The notification type handled by this notification handler*/
  notificationType: string

  /** The name of notification validators */
  validators: string[]

  /** The name of notification test cases */
  testCases: string[]

  /** The name of template providers */
  templates: string[]

  /** The name of this notification handler*/
  name: string

  /** The group name of this notification handler*/
  group: string

  /** The frequency metadata for sending notifications */
  transports: INotificationDispatchHandlerTransport[]
}

export interface INotificationHandlerConstructor {
  (decoration: INotificationHandlerDecoration): INotificationHandler<any>
}

export interface INotificationDispatchResult {
  transport: string;
  sent?: string[]
  failed?: { token: string, error: any }[]
  error?: any
  native?: any
}

export interface INotificationDispatchHandler {
  transportName: string;
  onDispatch(notification: INotification, notificationHandler: INotificationHandler<any>): Promise<INotificationDispatchResult>
  shutdown?(): void
}

export interface INotificationTransport<TNative> {
  shutdown(): void
  resolveTokens(recipients: NotificationRecipients): Promise<string[]>
  native: TNative
}

export interface INotificationHandler<TNotification extends INotification> {
  //[key: string] : PromiseOrType<>
}

/** The core Notification Recipient (a dictionary of transport tokens) */
export interface INotificationRecipient {
  //[tokenName: string]: ItemOrArray<string>
}

/** string values can only denote the default transport (only useful in single transport deployment) */
export type NotificationRecipients = ItemOrArray<INotificationRecipient | string>

/** The core Notification */
export interface INotification {
  /** The notification type identifier */
  type: string;
  /** The recipient(s) of the notification */
  recipient: NotificationRecipients
}
