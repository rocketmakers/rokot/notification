import * as Logger from "bunyan";
import { messageHandlerDecoration } from "./decorators";
import { INotification, INotificationHandler, INotificationDispatchResult, INotificationDispatchHandler, INotificationHandlerDecoration, INotificationHandlerConstructor } from "./core";
import { constructNotificationMessageHandler, resolvePromiseOrResult } from "./common";

export interface INotificationDispatcher {
  dispatch(notification: INotification): Promise<INotificationDispatchResult[]>
  shutdown(): void
}

export class NotificationDispatcher implements INotificationDispatcher {
  constructor(private logger: Logger, public dispatchHandlers: INotificationDispatchHandler[], private handlerClassConstructor?: INotificationHandlerConstructor) {
  }

  async dispatch(notification: INotification): Promise<INotificationDispatchResult[]> {
    this.logger.trace(`Dispatching notification type ${notification.type}`)
    this.validateMessageCore(notification);
    const decoration = this.findMessageHandlerDecoration(notification);
    const messageHandler = constructNotificationMessageHandler(decoration, this.handlerClassConstructor);
    const msg = await this.validateMessage(decoration, notification, messageHandler)
    const collect: INotificationDispatchResult[] = []
    for (const dispatchHandler of this.dispatchHandlers) {
      let result = await resolvePromiseOrResult(dispatchHandler.onDispatch(msg, messageHandler))
      if (result) {
        collect.push(result)
      } else {
        collect.push({ transport: dispatchHandler.transportName })
      }
    }
    return collect
  }

  shutdown() {
    this.dispatchHandlers.forEach(dh => dh.shutdown && dh.shutdown())
  }

  private validateMessageCore(notification: INotification) {
    if (!notification.type || !notification.recipient) {
      const msg = `Invalid Message payload - missing core requirements of 'type' and 'recipient': ${JSON.stringify(notification)}`;
      this.logger.error(msg)
      throw new Error(msg)
    }
  }

  private findMessageHandlerDecoration(notification: INotification) {
    const decoration = messageHandlerDecoration.get(notification.type);
    if (!decoration) {
      const msg = `No notification handler registered for notification type: ${notification.type}`;
      this.logger.error(msg)
      throw new Error(msg);
    }
    return decoration;
  }

  private async validateMessage<T extends INotification>(decoration: INotificationHandlerDecoration, notification: T, handler: INotificationHandler<any>) {
    this.logger.trace(`Checking validation for notification type ${notification.type}`)
    const validators = decoration.validators
    if (!validators || !validators.length) {
      this.logger.trace(`-- Not Required`)
      return notification;
    }

    for (let validate of validators) {
      this.logger.trace(`-- Validating with ${validate}`)
      notification = await resolvePromiseOrResult(handler[validate](notification))
    }

    return notification;
  }
}
