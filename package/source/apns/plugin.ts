import { ProviderOptions, Provider, Notification } from "apn";
import * as Logger from "bunyan";
import { IRecipientResolver, recipientTokenResolver } from "../core/recipients";
import { INotificationHandler, INotificationTransport, INotification, NotificationRecipients, INotificationDispatchResult } from "../core/core";
import { NotificationDispatchHandler } from "../core/dispatchHandler";

import { ResponseFailure } from "apn";

export type INotificationFailureError = Pick<ResponseFailure, "device" | "status" | "error">
export type INotificationFailureResponse = Pick<ResponseFailure, "device" | "status" | "response">

declare module "../core/core" {
  interface INotificationRecipient {
    apns?: string | string[]
  }
}

/** Implement this interface onto notification handlers that will use the nodemailer transport  */
export interface IApnsNotificationHandler<T extends INotification> extends INotificationHandler<T> {
  apns(notification: T, transport: IApnsNotificationTransport): Promise<INotificationDispatchResult>
}

export interface IApnsNotificationTransport extends INotificationTransport<Provider> {
  /** If the notification has valid recipient tokens, creates an apns message and sends via the native transport, otherwise undefined  */
  send(recipients: NotificationRecipients, builder: (notification: Notification) => void): Promise<INotificationDispatchResult>
}

export function isNotificationFailureError(failure: ResponseFailure): failure is INotificationFailureError {
  return !!(failure as INotificationFailureError).error
}

export function isNotificationFailureResponse(failure: ResponseFailure): failure is INotificationFailureResponse {
  return !!(failure as INotificationFailureResponse).response
}

export class ApnsNotificationDispatchHandler extends NotificationDispatchHandler<IApnsNotificationTransport> {
  constructor(logger: Logger, transport: IApnsNotificationTransport) {
    super("apns", logger, transport)
  }
}

export async function apnsInitializer(logger: Logger, options: ProviderOptions, recipientResolver: IRecipientResolver = recipientTokenResolver) {
  const transport = await ApnsNotificationTransport.create(logger, options, recipientResolver);
  return new ApnsNotificationDispatchHandler(logger, transport)
}

/** The Transport passed to apns notification handlers */
export class ApnsNotificationTransport implements IApnsNotificationTransport {

  static async create(logger: Logger, options: ProviderOptions, recipientResolver: IRecipientResolver): Promise<IApnsNotificationTransport> {
    const provider = new Provider(options);
    return new ApnsNotificationTransport(logger, provider, recipientResolver);
  }

  constructor(private logger: Logger, public native: Provider, private recipientResolver: IRecipientResolver) {
    logger.trace("Created Apns Notification Transport")
  }

  resolveTokens(recipients: NotificationRecipients): Promise<string[]> {
    return this.recipientResolver.find(recipients, "apns")
  }

  async send(recipients: NotificationRecipients, builder: (notification: Notification) => void) {
    const tokens = await this.resolveTokens(recipients)
    if (!tokens || !tokens.length) {
      return { transport: "apns", error: "No Tokens Found" } as INotificationDispatchResult;
    }
    const native = new Notification()
    builder && builder(native)
    return this.sendMessage(tokens, native)
  }

  private async sendMessage(tokens: string[], native: Notification): Promise<INotificationDispatchResult> {
    try {
      const resp = await this.native.send(native, tokens)
      this.logger.trace("Apns send response: ", resp && JSON.stringify(resp, null, 2))
      return { transport: "apns", sent: resp.sent.map(s => s.device), failed: resp.failed.map(f => ({ token: f.device, error: f })), native: resp };
    } catch (e) {
      this.logger.error(e, "Apns was unable to send notification");
      return { transport: "apns", error: e };
      //throw e
    }
  }

  shutdown() {
    this.native.shutdown()
  }
}
