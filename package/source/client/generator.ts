import * as Logger from "bunyan";
import { ApiReflector, IOptions } from "./reflector";
import * as fs from "fs";
import * as path from "path";

/**
converts the interface type name from 'IMyInterface' to 'myInterface'
useful for options param of generateClient (IOptions functionName)
*/
export function getInterfaceFunctionName(typeName: string, notificationType: string) {
  return `${typeName.substr(1, 1).toLowerCase()}${typeName.substr(2)}`
}

/**
Helper function to gather all files from a folder
Gets all the files from a relative folder path (relative to the root of the project)
useful for writeTo param of generateClient
*/
export function getFolderFiles(relativeFolder: string) {
  return fs.readdirSync(relativeFolder).map(f => "./" + path.join(relativeFolder, f))
}

/**
Generates a Notification Client - this is intended to be written inside the same project as the INotification definitions exist
all paths (files and writeTo) should be relative to the root of the project
*/
export function generateClient(logger: Logger, files: string[], writeTo: string, options?: IOptions) {
  const reflector = new ApiReflector(logger, files)
  reflector.writeNotifications(writeTo, options)
}
