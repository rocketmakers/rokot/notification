import * as Logger from "bunyan";
import { IRecipientResolver, recipientTokenResolver } from "../core/recipients";
import { INotification, NotificationRecipients, INotificationDispatchResult, INotificationHandler, INotificationTransport } from "../core/core";
import { NotificationDispatchHandler } from "../core/dispatchHandler";
import { MailService } from '@sendgrid/mail'
import { MailData } from "@sendgrid/helpers/classes/mail";
import * as sgMail from '@sendgrid/mail'
import { EmailData } from "@sendgrid/helpers/classes/email-address";

declare module "@sendgrid/helpers/classes/mail" {
  export interface MailData {
    dynamicTemplateData?: any
  }
}
declare module "../core/core" {
  interface INotificationRecipient {
    sendgrid?: string | string[]
    email?: string | string[]
  }
}

export class SendgridNotificationDispatchHandler extends NotificationDispatchHandler<ISendgridNotificationTransport> {
  constructor(logger: Logger, transport: ISendgridNotificationTransport) {
    super("sendgrid", logger, transport)
  }
}

export async function sendgridInitializer(logger: Logger, config: ISendgridOptions, recipientResolver?: IRecipientResolver) {
  const transporter = await new SendgridTransporter(logger, config).getTransporter();
  const transport = new SendgridNotificationTransport(logger, transporter, recipientResolver || recipientTokenResolver, config.defaultFromAddress);
  return new SendgridNotificationDispatchHandler(logger, transport)
}

export interface ISendgridNotification extends INotification {
  /** The 'from' email address */
  fromAddress?: string;
}

/** Implement this interface onto message handlers that will use the sendgrid transport  */
export interface ISendgridNotificationHandler<T extends ISendgridNotification> extends INotificationHandler<T> {
  sendgrid(notification: T, transport: ISendgridNotificationTransport): Promise<any>
}

/** The sendgrid transport */
export interface ISendgridNotificationTransport extends INotificationTransport<typeof MailService> {
  /** If the notification has valid recipient tokens, creates a sendgrid message with 'to' supplied, otherwise undefined  */
  send(recipients: NotificationRecipients, from: EmailData, builder: (message: MailData) => void): Promise<INotificationDispatchResult>

  /** Get either the specific 'from' email address for this notification, or uses the default */
  getFromAddress(notification: ISendgridNotification): string
}

/** sendgrid specific: Implement to provide a sendgrid Transporter plugin */
export class SendgridTransporter {
  constructor(private logger: Logger, private nodemailerConfig: ISendgridOptions) {
  }

  async getTransporter(): Promise<typeof MailService> {
    this.validate();
    return await this.createTransport();
  }

  private validate() {
    this.logger.trace(`Validating Sendgrid Transporter config`)
    if (!this.nodemailerConfig || !this.nodemailerConfig.apiKey) {
      const msg = "Email credential cannot be null/undefined or contain empty api key";
      this.logger.error("-- " + msg)
      throw new Error(msg)
    }

    this.logger.info("-- Passed")
  }

  private createSendgridTransport() {
    sgMail.setApiKey(this.nodemailerConfig.apiKey);
    const subs = this.nodemailerConfig.substitutionWrappers
    if (subs) {
      sgMail.setSubstitutionWrappers(subs[0], subs[1]); // Configure the substitution tag wrappers globally
    }

    return sgMail
  }

  private createTransport() {
    return new Promise<typeof MailService>((res, rej) => {
      res(this.createSendgridTransport());
    })
  }
}

export interface ISendgridOptions {
  apiKey: string;
  defaultFromAddress: string
  substitutionWrappers?: [string, string]
}

/** The Transport passed to message handlers implementing INodemailerMessageHandler<T>  */
export class SendgridNotificationTransport implements ISendgridNotificationTransport {

  static async create(logger: Logger, transporter: SendgridTransporter, recipientResolver: IRecipientResolver, defaultFromAddress: string): Promise<ISendgridNotificationTransport> {
    const t = await transporter.getTransporter();
    return new SendgridNotificationTransport(logger, t, recipientResolver, defaultFromAddress);
  }

  constructor(private logger: Logger, public native: typeof MailService, private recipientResolver: IRecipientResolver, private defaultFromAddress: string) {
    logger.trace("Created sendgrid Notification Transport")
  }

  getFromAddress(notification: ISendgridNotification): string {
    return notification.fromAddress || this.defaultFromAddress
  }

  shutdown() {
  }

  resolveTokens(recipients: NotificationRecipients): Promise<string[]> {
    return this.recipientResolver.find(recipients, ["sendgrid", "email"], true)
  }

  async send(recipients: NotificationRecipients, from: EmailData, builder: (message: MailData) => void) {
    const to = await this.resolveTokens(recipients)
    if (!to || !to.length) {
      return
    }
    const outMessage: MailData = { to, from }
    builder && builder(outMessage)
    return this.sendMessage(outMessage)
  }

  async sendMessage(mail: MailData): Promise<INotificationDispatchResult> {
    try {
      const info = await this.native.send(mail)
      //info[0].statusCode
      const failed = [];// info ? info.rejected.map(r => ({ token: r, error: null })) : []
      const sent = [];//info.accepted ? info.accepted : []
      return { transport: "sendgrid", sent, failed, native: info }
    } catch (e) {
      this.logger.error(e, "sendgrid was unable to send mail");
      return { transport: "sendgrid", error: e };
    }
  }
}
