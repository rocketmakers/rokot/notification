import * as nodemailer from 'nodemailer';
import * as Logger from "bunyan";
import { IRecipientResolver, recipientTokenResolver } from "../core/recipients";
import { INotification, NotificationRecipients, INotificationDispatchResult, INotificationHandler, INotificationTransport } from "../core/core";
import { NotificationDispatchHandler } from "../core/dispatchHandler";

declare module "../core/core" {
  interface INotificationRecipient {
    nodemailer?: string | string[]
    email?: string | string[]
  }
}

export class NodemailerNotificationDispatchHandler extends NotificationDispatchHandler<INodemailerNotificationTransport> {
  constructor(logger: Logger, transport: INodemailerNotificationTransport) {
    super("nodemailer", logger, transport)
  }
}

export async function nodemailerInitializer(logger: Logger, transporterProvider: NodemailerTransporterBase, defaultFromAddress: string, recipientResolver?: IRecipientResolver) {
  const transporter = await transporterProvider.getTransporter();
  const transport = new NodemailerNotificationTransport(logger, transporter, recipientResolver || recipientTokenResolver, defaultFromAddress);
  return new NodemailerNotificationDispatchHandler(logger, transport)
}

export interface INodemailerNotification extends INotification {
  /** The 'from' email address */
  fromAddress?: string;
}

/** Implement this interface onto message handlers that will use the nodemailer transport  */
export interface INodemailerNotificationHandler<T extends INodemailerNotification> extends INotificationHandler<T> {
  nodemailer(notification: T, transport: INodemailerNotificationTransport): Promise<any>
}

/** The nodemailer transport */
export interface INodemailerNotificationTransport extends INotificationTransport<nodemailer.Transporter> {
  /** If the notification has valid recipient tokens, creates a nodemailer message with 'to' supplied, otherwise undefined  */
  send(recipients: NotificationRecipients, builder: (message: nodemailer.SendMailOptions) => void): Promise<INotificationDispatchResult>

  /** Get either the specific 'from' email address for this notification, or uses the default */
  getFromAddress(notification: INodemailerNotification): string
}

/** Nodemailer specific: Implement to provide a nodemailer Transporter plugin */
export abstract class NodemailerTransporterBase {
  async getTransporter(): Promise<nodemailer.Transporter> {
    this.validate();
    return await this.createTransport();
  }

  protected abstract validate();

  protected abstract createNodemailerTransport(): nodemailer.Transport;

  private createTransport() {
    return new Promise<nodemailer.Transporter>((res, rej) => {
      res(nodemailer.createTransport(this.createNodemailerTransport()));
    })
  }
}
/** The Transport passed to message handlers implementing INodemailerMessageHandler<T>  */
export class NodemailerNotificationTransport implements INodemailerNotificationTransport {

  static async create(logger: Logger, transporter: NodemailerTransporterBase, recipientResolver: IRecipientResolver, defaultFromAddress: string): Promise<INodemailerNotificationTransport> {
    const t = await transporter.getTransporter();
    return new NodemailerNotificationTransport(logger, t, recipientResolver, defaultFromAddress);
  }

  constructor(private logger: Logger, public native: nodemailer.Transporter, private recipientResolver: IRecipientResolver, private defaultFromAddress: string) {
    logger.trace("Created Nodemailer Notification Transport")
  }

  getFromAddress(notification: INodemailerNotification): string {
    return notification.fromAddress || this.defaultFromAddress
  }

  shutdown() {
    this.native.close()
  }

  resolveTokens(recipients: NotificationRecipients): Promise<string[]> {
    return this.recipientResolver.find(recipients, ["nodemailer", "email"], true)
  }

  async send(recipients: NotificationRecipients, builder: (message: nodemailer.SendMailOptions) => void) {
    const to = await this.resolveTokens(recipients)
    if (!to || !to.length) {
      return
    }
    const outMessage: nodemailer.SendMailOptions = { to }
    builder && builder(outMessage)
    return this.sendMessage(outMessage)
  }

  async sendMessage(mail: nodemailer.SendMailOptions): Promise<INotificationDispatchResult> {
    try {
      const info = await this.native.sendMail(mail)
      const failed = info.rejected ? info.rejected.map(r => ({ token: r, error: null })) : []
      const sent = info.accepted ? info.accepted : []
      return { transport: "nodemailer", sent, failed, native: info }
    } catch (e) {
      this.logger.error(e, "Nodemailer was unable to send mail");
      return { transport: "nodemailer", error: e };
    }
  }
}
