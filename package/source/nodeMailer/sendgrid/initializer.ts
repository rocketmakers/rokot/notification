import { INodemailerSendgridOptions, NodemailerSendgridTransporter } from "./transporter";
import * as Logger from "bunyan";
import { nodemailerInitializer } from "../plugin";
import { IRecipientResolver } from "../../core/recipients";

export async function sendgridInitializer(logger: Logger, config: INodemailerSendgridOptions, recipientResolver?: IRecipientResolver) {
  return nodemailerInitializer(logger, new NodemailerSendgridTransporter(logger, config), config.defaultFromAddress, recipientResolver)
}
