import { NodemailerTransporterBase } from "../plugin";
import * as Logger from "bunyan";
var sgTransport = require('nodemailer-sendgrid-transport');

export interface INodemailerSendgridOptions {
  username?: string;
  password: string;
  defaultFromAddress: string
}

/** Nodemailer specific: Implementation of the nodemailer Sendgrid Transporter plugin */
export class NodemailerSendgridTransporter extends NodemailerTransporterBase {
  constructor(private logger: Logger, private nodemailerConfig: INodemailerSendgridOptions) {
    super()
  }

  protected validate() {
    this.logger.trace(`Validating Nodemailer Sendgrid Transporter config`)
    if (!this.nodemailerConfig || !this.nodemailerConfig.password) {
      const msg = "Email credential cannot be null/undefined or contain empty username and/or password";
      this.logger.error("-- " + msg)
      throw new Error(msg)
    }

    this.logger.info("-- Passed")
  }

  protected createNodemailerTransport() {
    const auth: any = {
      api_key: this.nodemailerConfig.password
    }

    if (this.nodemailerConfig.username) {
      auth.api_user = this.nodemailerConfig.username
    }
    return sgTransport({
      auth
    })
  }
}
