export { NotificationRecipients, INotificationRecipient, INotification, INotificationHandlerDecoration, INotificationDispatchHandlerTransport, PromiseOrResult, ItemOrArray, Frequency, AllFrequency, INewable, INotificationDispatchHandler, INotificationHandlerConstructor, INotificationHandler, INotificationDispatchResult } from "./core/core";
export { createNotificationMessage } from "./core/common";
export { recipientTokenResolver, IRecipientResolver } from "./core/recipients";
export { notifications, messageHandlerDecoration } from "./core/decorators";
export { NotificationDispatcher, INotificationDispatcher } from "./core/dispatcher";
export { NotificationDispatcherFactory } from "./core/dispatcherFactory";

export { ITemplateContentProvider, ITemplateProcessor, INestingTemplateModel, ITemplateProcessorFactory } from "./templates/core";
export { FileSystemContent } from "./templates/fileSystemContent";
export { NestedTemplateProcessor } from "./templates/nestedTemplateProcessor";
export { TemplatePreProcessor } from "./templates/templatePreProcessor";

export { UnderscoreTemplateProcessorFactory, UnderscoreFileSystemNestedTemplateProcessor, UnderscoreFileSystemTemplateProcessorFactory } from "./templates/underscore";
