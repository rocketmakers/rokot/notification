import * as Logger from "bunyan";
import { templates } from "../core/templates";
import { PromiseOrResult, INotificationHandlerConstructor, INotificationHandler, INotificationHandlerDecoration } from "../core/core";
import { resolvePromiseOrResult, constructNotificationMessageHandler } from "../core/common";
import { ITemplateProcessor, ITemplateProcessorProviderFunction } from "./core";
import 'reflect-metadata';

export interface ICachedTemplate {
  func: ITemplateProcessorProviderFunction<any>
  proc: ITemplateProcessor<any>
}

export interface ITemplatePreProcessorConfig {
  onTemplateCached?(notificationType: string, templateName: string, func: ITemplateProcessorProviderFunction<any>, proc: ITemplateProcessor<any>): void
  onTemplateRendered?(notificationType: string, templateName: string, testCase: string, content: string): void
}

export class TemplatePreProcessor {
  constructor(private logger: Logger, private handlerClassConstructor?: INotificationHandlerConstructor) { }

  private runSampleMessageRender(decoration: INotificationHandlerDecoration, templateName: string, handler: INotificationHandler<any>, proc: ITemplateProcessor<any>, config: ITemplatePreProcessorConfig) {
    const testCases = decoration.testCases;
    if (!testCases || !testCases.length) {
      this.logger.warn(`-- -- -- No Test Case functions defined`)
      return;
    }
    for (let testCase of testCases) {
      try {
        const sm = handler[testCase]([templateName]);
        if (!sm) {
          continue;
        }
        const content = proc(sm)
        const message = `-- -- -- compiled template from notification '${testCase}'`
        this.logger.trace(message)
        //this.logger.info(content)
        config && config.onTemplateRendered && config.onTemplateRendered(decoration.notificationType, templateName, testCase, content)
      } catch (e) {
        const message = `-- -- -- failed to compile template from notification '${testCase}'`
        this.logger.error(e, message)
        return message
      }
    }
  }

  async preProcess(decorations: INotificationHandlerDecoration[], config?: ITemplatePreProcessorConfig) {
    const errors: string[] = [];
    this.logger.trace(`Pre Processing Message Handlers Template Compiler`)
    for (const decoration of decorations) {
      this.logger.trace(`-- Processing ${decoration.notificationType}`)
      const handler = constructNotificationMessageHandler(decoration, this.handlerClassConstructor);
      for (const templateName of decoration.templates) {
        this.logger.trace(`-- -- template: ${templateName}`)
        let promise = handler[templateName]() as PromiseOrResult<ITemplateProcessor<any>>;
        let proc = await resolvePromiseOrResult(promise);
        config && config.onTemplateCached && config.onTemplateCached(decoration.notificationType, templateName, handler[templateName], proc)
        templates.set(proc, handler[templateName])
        const msg = this.runSampleMessageRender(decoration, templateName, handler, proc, config)
        if (msg) {
          errors.push(msg)
        }
      }
    }
    if (errors.length) {
      throw new Error(errors.join("; "))
    }
  }
}
