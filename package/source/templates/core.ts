import { PromiseOrResult } from "../core/core";

/**
Resolves raw (pre-processed) template content (typically from a store/disk)
*/
export interface ITemplateContentProvider {
  /** Gets the raw (pre-processed) template for the supplied template key*/
  get(templateKey: string): Promise<string>;
}

/**
Processes models to produce textual output
*/
export interface ITemplateProcessor<TModel> {
  /** takes a model and produces output text */
  (model: TModel): string;
}

/**
Factory to creates Template Processors based on template keys
*/
export interface ITemplateProcessorFactory {
  /**
  Creates a template Template Processor for the provided key
  */
  create<TModel>(templateKey: string): Promise<ITemplateProcessor<TModel>>;
}

export type ITemplateProcessorProviderFunction<T> = () => PromiseOrResult<ITemplateProcessor<T>>

/** Represents the model a Nesting Template */
export interface INestingTemplateModel {
  /** the body content of a Nesting Template */
  body: string;
}
