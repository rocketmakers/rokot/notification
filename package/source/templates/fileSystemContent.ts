import * as path from "path";
import { ITemplateContentProvider } from "./core";
import { Logger } from "rokot-log";
import { AsyncFs } from "./asyncFs";

/**
A File System template content provider
Resolves pre-processed template content (reading from a base path)
*/
export class FileSystemContent implements ITemplateContentProvider {
  /** supply a base path */
  constructor(private basePath: string, private logger: Logger) { }
  /** Gets the raw (pre-processed) content for the supplied template key (template key is the path relative to base path) */
  async get(templateKey: string): Promise<string> {
    const filePath = path.join(this.basePath, templateKey);
    const exists = await AsyncFs.existsAsync(filePath)
    if (!exists) {
      this.logger.warn(`An email template has been Generated: File not found '${filePath}' for notification template ${templateKey}`)
      return `/* AUTOGEN: ${templateKey}  */<p>${templateKey}</p>`
    }

    try {
      return await AsyncFs.readFileAsync(filePath)
    } catch (err) {
      this.logger.error(`Unable to read '${filePath}' for notification template ${templateKey}: ${err}`)
      throw err
    }
  }
}
