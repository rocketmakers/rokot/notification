import * as _ from "underscore";
import * as path from "path";
import { ITemplateContentProvider, ITemplateProcessor, ITemplateProcessorFactory } from "./core";
import { NestedTemplateProcessor } from "./nestedTemplateProcessor";
import { FileSystemContent } from "./fileSystemContent";
import { Logger } from "rokot-log";
import { AsyncFs } from "./asyncFs";

export type ITemplateFragmentProvider = { [name: string]: ITemplateProcessor<any> }

/** Creates Template Processors */
export class UnderscoreTemplateProcessorFactory implements ITemplateProcessorFactory {
  constructor(private contentProvider: ITemplateContentProvider, private fragmentProvider?: () => Promise<ITemplateFragmentProvider>) { }
  /** Create a Template Processor for the supplied template key*/
  async create<T>(templateKey: string): Promise<ITemplateProcessor<T>> {
    if (this.fragmentProvider) {
      return this.createWithFragment(templateKey)
    }
    return this.createWithoutFragment(templateKey)
  }

  protected async createWithFragment<T>(templateKey: string): Promise<ITemplateProcessor<T>> {
    const c = await this.contentProvider.get(templateKey)
    const fragments = await this.fragmentProvider()
    return (m: T) => _.template(c)({ model: m, fragments })
  }

  protected async createWithoutFragment<T>(templateKey: string): Promise<ITemplateProcessor<T>> {
    const c = await this.contentProvider.get(templateKey)
    return this.createTemplateProcessorWithoutFragment<T>(c)
  }

  protected async createTemplateProcessorWithoutFragment<T>(content: string): Promise<ITemplateProcessor<T>> {
    return _.template(content, { variable: "model" })
  }
}

/** Nesting Underscore templates, Read from file system */
export class UnderscoreFileSystemNestedTemplateProcessor extends NestedTemplateProcessor {
  constructor(public basePath: string, logger: Logger, fragmentsRelativeFolder?: string) {
    super(new UnderscoreFileSystemTemplateProcessorFactory(basePath, logger, fragmentsRelativeFolder))
  }
}

/** Underscore templates, Read from file system */
export class UnderscoreFileSystemTemplateProcessorFactory extends UnderscoreTemplateProcessorFactory {
  /** Supply a base path */
  constructor(public basePath: string, private logger: Logger, fragmentsRelativeFolder?: string) {
    super(new FileSystemContent(basePath, logger), fragmentsRelativeFolder && (() => this.fragmentProviderFactory(fragmentsRelativeFolder)))
  }

  private cachedFragments: ITemplateFragmentProvider
  private async fragmentProviderFactory(fragmentsRelativeFolder: string) {
    if (!_.isUndefined(this.cachedFragments)) {
      return this.cachedFragments
    }

    const fragmentsPath = path.join(this.basePath, fragmentsRelativeFolder)
    if (!await AsyncFs.existsAsync(fragmentsPath)) {
      this.logger.error(`Unable to read template fragment path '${fragmentsPath}'`)
      return this.cachedFragments = null
    }

    const fragmentFiles = await AsyncFs.readdirAsync(fragmentsPath)
    const fragments: ITemplateFragmentProvider = {}
    for (const file of fragmentFiles) {
      const template = await this.createWithoutFragment(fragmentsRelativeFolder + "/" + file)
      fragments[file] = template
    }

    return this.cachedFragments = fragments
  }
}