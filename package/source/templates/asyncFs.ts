import * as fs from "fs";
import * as path from "path";

export class AsyncFs {
  static statSizeAsync(filename: string) {
    return new Promise<number>((res, rej) => {
      fs.stat(filename, (err, stat) => {
        if (err) {
          return rej(err);
        }
        res(stat.size)
      })
    })
  }

  static readdirAsync(folder: string) {
    return new Promise<string[]>((res, rej) => {
      fs.readdir(folder, (err, files) => {
        if (err) {
          return rej(err);
        }

        res(files)
      })
    })
  }

  static existsAsync(filePathOrFolder: string) {
    return new Promise<boolean>((res, rej) => {
      fs.exists(filePathOrFolder, exists => res(exists))
    })
  }

  static readFileAsync(filePath: string) {
    return new Promise<string>((res, rej) => {
      fs.readFile(filePath, {}, (err, data) => {
        if (err) {
          return rej(err);
        }

        return res(data.toString());
      });
    })
  }

  static writeFileAsync(filePath: string, content: string | Buffer) {
    return new Promise<void>((res, rej) => {
      fs.writeFile(filePath, content, err => {
        if (err) {
          return rej(err);
        }

        return res();
      });
    })
  }

  static appendFileAsync(filePath: string, content: string | Buffer) {
    return new Promise<void>((res, rej) => {
      fs.appendFile(filePath, content, err => {
        if (err) {
          return rej(err);
        }

        return res();
      });
    })
  }

  static mkdirAsync(folder: string) {
    return new Promise<void>((res, rej) => {
      fs.mkdir(folder, err => {
        if (err) {
          rej(err)
          return
        }
        res()
      })
    })
  }

  static async mkdirRecursiveAsync(folder: string) {
    const exists = await this.existsAsync(folder)
    if (exists) {
      return;
    }
    await this.mkdirRecursiveAsync(path.dirname(folder))
    console.log(`-- Creating ${folder}`)
    await this.mkdirAsync(folder)
  }

  static mkfiledirRecursiveAsync(filename: string): Promise<void> {
    return this.mkdirRecursiveAsync(path.dirname(filename))
  }

  static async writeFileSafeAsync(filePath: string, content: string | Buffer) {
    await this.mkfiledirRecursiveAsync(filePath)
    await this.writeFileAsync(filePath, content)
  }
}
