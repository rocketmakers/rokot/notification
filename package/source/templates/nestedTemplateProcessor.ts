import * as _ from "underscore";
import { ITemplateProcessor, INestingTemplateModel, ITemplateProcessorFactory } from "./core";

/**
The Template Transformer
Nests template content inside a nesting parent template: INestingTemplateModel
*/
export class NestedTemplateProcessor {
  constructor(private templateProcessorFactory: ITemplateProcessorFactory) { }

  /** combines the template inside the nesting template */
  async combine<T>(templateKey: Promise<ITemplateProcessor<T>> | string, nestingTemplateKey: string = "template-layout"): Promise<ITemplateProcessor<T>> {
    const nestingTemplateProcessor = await this.templateProcessorFactory.create<INestingTemplateModel>(nestingTemplateKey)
    const templateProcessor = await (_.isString(templateKey) ? this.templateProcessorFactory.create<T>(templateKey) : templateKey)
    return (m: T) => nestingTemplateProcessor({ body: templateProcessor(m) })
  }
}
