const fs = require("fs")
const events = require("events")

function createLineReader(fileName) {
    var EM = events.EventEmitter
    var ev = new EM()
    var stream = fs.createReadStream(fileName)
    var remainder = null;
    stream.on("data", function(data) {
        if (remainder != null) { //append newly received data chunk
            var tmp = new Buffer(remainder.length + data.length)
            remainder.copy(tmp)
            data.copy(tmp, remainder.length)
            data = tmp;
        }
        var start = 0;
        for (var i = 0; i < data.length; i++) {
            if (data[i] == 10) { //\n new line
                var line = data.slice(start, i)
                ev.emit("line", line)
                start = i + 1;
            }
        }
        if (start < data.length) {
            remainder = data.slice(start);
        } else {
            remainder = null;
        }
    })

    stream.on("end", function() {
        if (null != remainder) ev.emit("line", remainder)
        ev.emit("end")
    })

    return ev
}

const r = createLineReader("./readme.template.md")
const w = fs.createWriteStream("./readme.md")
r.on("line", (line) => {
  const ln = line.toString()
  if (ln.indexOf("__LOADER__:") === 0) {
    const parts = ln.substring(11).split("|")
    if (!parts.length) {
      console.log("ARGHHHHH")
      return;
    }

    const options = parts[1] ? JSON.parse(parts[1]) : {}
    const file = fs.readFileSync(parts[0])
    let fileContent = file.toString()
    options.replace && options.replace.forEach(r => {
        fileContent = fileContent.replace(r[0], r[1])
    })
    const ok = w.write(fileContent, "UTF8")
    //console.log("WRITE MODIFIED", ok)
    return;
  }
  const ok = w.write(ln + "\n", "UTF8")
  //console.log("WRITE", ok)
})
r.on("end", function() {
  w.end()
  fs.writeFileSync("./package/readme.md", fs.readFileSync("./readme.md"))
  console.log("DONE!")
})
//w.end()
