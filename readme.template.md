# rokot-notification

Rokot - [Rocketmakers](http://www.rocketmakers.com/) TypeScript NodeJs Platform

## Introduction

The Rokot Notification library provides a framework to create (JSON) Notification Messages and send them to users via a notification channel (`email`, `apns` and `gcm` 'out-the-box')

These notification messages are generally transcoded into some form of textual content (HTML/text), but could also directly contain the content to be sent

### Core Components

#### INotification

The library is based around the core concept of a notification message (`INotification`)

This should represent the real life data behind the user notification

Each notification should have:
1. a unique `type` (this is the key to indicate which handler will process the notification)
1. a `recipient` to indicate the audience of the notification - which can be string values (for use in a single transport scenario), or transport keys `{email:"developer@code.com", apns:["ABC", "XYYZ"], gcm:"xds")` to support multiple transports and multiple tokens

```typescript
__LOADER__:./package/source/test/examples/handlers/sampleTemplateNotification.ts|{"replace":[["../../../index","rokot-notification"],["../../../nodemailer","rokot-notification/lib/nodemailer"],["../../../apns","rokot-notification/lib/apns"]]}
```

```typescript
const notification: ISampleTemplateNotification = {
  type: "sample-underscore-template-message",
  recipient: {
    email:"developers@rocketmakers.com"
  },
  fail: false
}
```

#### INotificationHandler

Each notification `type` requires a notification handler

The framework provides this handler with decorators to add functions to support:
1. validating notifications before the transport method is called
1. creating template content (html/text content via underscore templates)
1. to pre cache the templates at startup (loading template content from disk/db early)
1. to validate the template execution at startup with test case notifications you provide

A simple notification handler that just utilise the transport

```typescript
__LOADER__:./package/source/test/examples/handlers/simple.ts|{"replace":[["../../../index","rokot-notification"],["../../../nodemailer","rokot-notification/lib/nodemailer"],["../../../apns","rokot-notification/lib/apns"],["../../../gcm","rokot-notification/lib/gcm"]]}
```

Or a complete example showing usage of all the functions

```typescript
__LOADER__:./package/source/test/examples/handlers/template.ts|{"replace":[["../../../index","rokot-notification"],["../../../nodemailer","rokot-notification/lib/nodemailer"],["../../../apns","rokot-notification/lib/apns"],["../../../gcm","rokot-notification/lib/gcm"]]}
```

Or a notification handler using multiple transports

```typescript
__LOADER__:./package/source/test/examples/handlers/multiple.ts|{"replace":[["../../../index","rokot-notification"],["../../../nodemailer","rokot-notification/lib/nodemailer"],["../../../apns","rokot-notification/lib/apns"],["../../../gcm","rokot-notification/lib/gcm"]]}
```

NOTE: There are 3 core notification handlers avaiable 'out-the-box' `IApnsNotificationHandler<TNotification>`, `IGcmNotificationHandler<TNotification>` and `INodemailerNotificationHandler<TNotification>`

#### NotificationDispatcher

The framework provides a `NotificationDispatcher` to route notifications to their dispatch handler

You need to create your Application `NotificationDispatcher` and configuring which transports are used

```typescript
__LOADER__:./package/source/test/examples/dispatchers/multipleHandlers.ts|{"replace":[["../../../index","rokot-notification"],["../../../nodemailer","rokot-notification/lib/nodemailer"],["../../../apns","rokot-notification/lib/apns"],["../../../gcm","rokot-notification/lib/gcm"]]}
```

NOTE: You can create your own MessageHandler for any kind of notification channel you like, or change the Nodemailer transport (from the 'out-the-box' sendgrid)

#### Notification Client Generation

You can create a typescript file in the root of your source and include the source below (modifying the paths to match your handler locations)

This code will inspect the source code and generate a Notifications client - exposing methods for each of the notification types

```typescript
__LOADER__:./package/source/test/clientGenerator.ts|{"replace":[["../client/generator","rokot-notification/lib/client/generator"]]}
```

#### Templates

HTML Template

```html
__LOADER__:./package/source/test/examples/templates/sample-underscore-template-message.html
```

## Getting Started

### Installation

Install via `yarn`

```
yarn add rokot-notification
```

## Further Examples

Check out the examples in `/package/source/test/examples` to see various dispatcher configs and message handlers

## Consumed Libraries

### [node-gcm](https://github.com/ToothlessGear/node-gcm)
Android Push Notifications
### [apn](https://github.com/node-apn/node-apn/)
Apple Push Notifications
### [nodemailer](https://github.com/nodemailer/nodemailer)
Email Notifications
### [nodemailer-sendgrid-transport](https://github.com/sendgrid/nodemailer-sendgrid-transport)
Email via sendgrid transport

### [rokot-test](https://github.com/Rocketmakers/rokot-test)

The testing framework used within the Rokot Platform!
